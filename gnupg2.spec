Summary:       A complete implementation of the OpenPGP to encrypt and sign
Name:          gnupg2
Version:       2.4.3
Release:       4%{?dist}
License:       GPLv3+
URL:           https://www.gnupg.org/
Source0:       https://gnupg.org/ftp/gcrypt/gnupg/gnupg-%{version}.tar.bz2
Patch3000:     gnupg-2.1.10-secmem.patch
Patch3001:     gnupg-2.1.1-fips-algo.patch
Patch3002:     gnupg-2.2.23-large-rsa.patch
# fix missing uid on refresh from keys.openpgp.org
# https://salsa.debian.org/debian/gnupg2/commit/f292beac1171c6c77faf41d1f88c2e0942ed4437
Patch3003:    gnupg-2.2.18-tests-add-test-cases-for-import-without-uid.patch
Patch3004:    gnupg-2.4.0-gpg-allow-import-of-previously-known-keys-even-without-UI.patch
Patch3005:    gnupg-2.2.18-gpg-accept-subkeys-with-a-good-revocation-but-no-self-sig.patch
# Coverity scan fixes
Patch3006:    gnupg-2.2.21-coverity.patch


BuildRequires: gcc make
BuildRequires: bzip2-devel curl-devel zlib-devel
BuildRequires: openldap-devel pcsc-lite-libs npth-devel gnutls-devel
BuildRequires: readline-devel ncurses-devel sqlite-devel
BuildRequires: libassuan-devel >= 2.5.0
BuildRequires: libgcrypt-devel >= 1.9.1
BuildRequires: libgpg-error-devel >= 1.46
BuildRequires: libksba-devel >= 1.6.3
BuildRequires: docbook-utils gettext
BuildRequires: fuse
# for tests
BuildRequires: openssh-clients

Requires:      libgcrypt >= 1.9.1
Requires:      libgpg-error >= 1.46
Recommends:    pinentry gnupg2-smime
# for USB smart card support
Recommends:    pcsc-lite-ccid
Provides:      dirmngr = %{version}-%{release}
Provides:      gpg = %{version}-%{release}
Provides:      gnupg = %{version}-%{release}

%description
GnuPG is a complete and free implementation of the OpenPGP standard as defined by
RFC4880 (also known as PGP). GnuPG enables encryption and signing of data and communication,
and features a versatile key management system as well as access modules for public key directories.

GnuPG is a command line tool without any graphical user interface. It is an universal crypto engine
which can be used directly from a command line prompt, from shell scripts, or from other programs.
Therefore GnuPG is often used as the actual crypto backend of other applications.

Starting with version 2 GnuPG provides support for S/MIME and Secure Shell in addition to OpenPGP.


%package smime
Summary:       CMS encryption and signing tool and smart card support for GnuPG
Requires:      gnupg2 = %{version}-%{release}

%description smime
This package adds support for smart cards and S/MIME encryption and signing
to the base GnuPG package 


%package doc
Summary:       Documents for GnuPG

%description doc
This package contains FAQ and examples for gpg and so on.


%prep
%autosetup -n gnupg-%{version} -p1


%build
%global pcsclib %(basename $(ls -1 %{_libdir}/libpcsclite.so.? 2>/dev/null ) 2>/dev/null )
sed -i -e 's/"libpcsclite\.so"/"%{pcsclib}"/' scd/scdaemon.c

%configure --disable-rpath \
           --enable-gpg-is-gpg2 \
           --enable-g13 \
           --disable-ccid-driver \
           --enable-large-secmem \
           --docdir=%{_pkgdocdir}

%make_build


%install
%make_install

mkdir -p %{buildroot}%{_sysconfdir}/gnupg
touch %{buildroot}%{_sysconfdir}/gnupg/gpgconf.conf

# compat symlinks
ln -sf gpg2 %{buildroot}%{_bindir}/gpg
ln -sf gpgv2 %{buildroot}%{_bindir}/gpgv
ln -sf gpg2.1 %{buildroot}%{_mandir}/man1/gpg.1
ln -sf gpgv2.1 %{buildroot}%{_mandir}/man1/gpgv.1

rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_bindir}/gpgscm

%find_lang %{name}


%check
mkdir -p $HOME/.gnupg
pushd ./bin
ln -s gpg gpg2
popd
make check


%files -f %{name}.lang
%license COPYING*
%doc AUTHORS NEWS README THANKS TODO
%dir %{_sysconfdir}/gnupg
%ghost %config(noreplace) %{_sysconfdir}/gnupg/gpgconf.conf
%{_bindir}/gpg
%{_bindir}/gpgv
%{_bindir}/gpg2
%{_bindir}/gpgv2
%{_bindir}/gpg-card
%{_bindir}/gpg-connect-agent
%{_bindir}/gpg-agent
%{_bindir}/gpg-wks-client
%{_bindir}/gpgconf
%{_bindir}/gpgparsemail
%{_bindir}/gpgtar
%{_bindir}/g13
%{_bindir}/dirmngr
%{_bindir}/dirmngr-client
%{_bindir}/watchgnupg
%{_bindir}/gpg-wks-server
%{_bindir}/gpgsplit
%{_sbindir}/*
%{_datadir}/gnupg/
%{_libexecdir}/*
%{_infodir}/*.info*
%{_mandir}/man?/*
%exclude %{_mandir}/man?/gpgsm*


%files smime
%{_bindir}/gpgsm*
%{_bindir}/kbxutil
%{_mandir}/man?/gpgsm*


%files doc
%{_pkgdocdir}
%exclude %{_pkgdocdir}/README


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.3-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.3-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.3-2
- Rebuilt for OpenCloudOS Stream 23.09

* Tue Aug 15 2023 Miaojun Dong <zoedong@tencent.com> - 2.4.3-1
- Bump version to 2.4.3

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.3.7-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.3.7-2
- Rebuilt for OpenCloudOS Stream 23

* Wed Jul 13 2022 rockerzhu <rockerzhu@tencent.com> - 2.3.7-1
- Initial build
